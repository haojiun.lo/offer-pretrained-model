import os
import re
import argparse

from pyspark.sql import functions as F
from pyspark.sql.types import ArrayType, IntegerType

from utils.spark_utils import get_stand_alone_spark
from utils.logging_conf import get_logger
from utils import s3_utils
from utils.env import APP_HOME
from utils.encoder import get_encoder


def build_clean_char_re_str(char_list):
    re_char = "["

    if "|" not in char_list:
        char_list.append("|")
    if "\n" not in char_list:
        char_list.append("\n")

    for c in char_list:
        if c == "[":
            re_char += "\\["
        elif c == "]":
            re_char += "\\]"
        elif c == "^":
            re_char += "\\^"
        elif c == "-":
            re_char += "\\-"
        else:
            re_char += c

    re_char += "]"

    return re_char


def read_char_list(path):
    if path and path.startswith("s3"):
        s3_path = s3_utils.convert_to_s3a(path)
        bucket, key = s3_utils.extract_bucket_and_key(s3_path)
        data = s3_utils.s3_read_object(bucket, key)
        char_list = data.decode('utf-8').split("\x01")
    else:
        # read as local path
        with open(path, "r") as f:
            char_list = f.read().split("\x01")

    return char_list


def read_bpe_merge_from_s3(path):
    if path and path.startswith("s3"):
        s3_path = s3_utils.convert_to_s3a(path)
        bucket, key = s3_utils.extract_bucket_and_key(s3_path)
        data = s3_utils.s3_read_object(bucket, key)
        temp_path = os.path.join(APP_HOME, "temp_bpe", "bpe_merges.txt")
        os.makedirs(os.path.dirname(temp_path), exist_ok=True)
        with open(temp_path, "wb") as f:
            f.write(data)
    else:
        temp_path = path

    return temp_path


def get_tokenize_udf(bpe_merge_path, to_clean_char_path, merge_num=40000):
    bpe_path = read_bpe_merge_from_s3(bpe_merge_path)
    encoder = get_encoder(bpe_path, merge_num=merge_num)
    char_list = read_char_list(to_clean_char_path)
    re_str = build_clean_char_re_str(char_list)
    re_com = re.compile(re_str)

    def tokenize(inp_str):
        # localize object
        enc = encoder
        re_compile = re_com
        if inp_str is None:
            return None

        # preprocess
        inp_str = inp_str.lower().strip()
        inp_str = re_compile.sub(" ", inp_str)
        inp_str = re.sub("[ ]{2,}", " ", inp_str).strip()

        # tokenize
        enc_list = enc.encode(inp_str)

        return enc_list

    udf = F.udf(tokenize, ArrayType(IntegerType()))

    return udf


def main(spark, logger,
         input_path,
         output_path,
         to_clean_char_file_path=None,
         bpe_merge_path=None,
         output_format="tfrecord"):
    """Clean char in 'to_clean_char_file_path', replace with white space, and tokenize with BPE.
    Args:
        spark: spark session.
        logger: python logger.
        input_path: S3 path, json2 in parquet format.
        output_path: S3 path, output result in 'output_format' format.
        to_clean_char_file_path: S3 or local path[optional], path to char list txt file.
            format: each char in file seperated with special char '\x01'.
        bpe_merge_path: S3 path[optional], bpe_merges.txt file path. If none using the one in 'resources'.
        output_format: string, output format, default: 'parquet'.
            Availible format: 'tfrecord', 'parquet', 'json', 'csv'
    """
    logger.info("Input args:")
    logger.info(f"input_path: {input_path}")
    logger.info(f"output_path: {output_path}")
    logger.info(f"to_clean_char_file_path: {to_clean_char_file_path}")
    logger.info(f"output_format: {output_format}")
    if not output_format:
        logger.info(f"output_format is empty: {output_format}")
        output_format = "tfrecord"
        logger.info(f"Using output_format as: {output_format}")
    if not to_clean_char_file_path:
        logger.info(f"to_clean_char_file_path is empty: {to_clean_char_file_path}")
        to_clean_char_file_path = os.path.join(APP_HOME, "resources/to_clean_char_list.txt")
        logger.info(f"Read from local path: {to_clean_char_file_path}")
    if not bpe_merge_path:
        logger.info(f"bpe_merge_path is empty: {bpe_merge_path}")
        bpe_merge_path = os.path.join(APP_HOME, "resources/bpe_merges.txt")
        logger.info(f"Read from local path: {bpe_merge_path}")

    udf = get_tokenize_udf(bpe_merge_path, to_clean_char_file_path)
    df = spark.read.parquet(input_path)

    tokenize_df = (
        df
        .select(
            F.col("groupId").alias("offer_id"),
            "merchant",
            "title",
            "x_cate1",
            "x_desc1",
            F.trim(F.lower("brand")).alias("brand"),
            udf("title").alias("title_enc"),
            udf("x_cate1").alias("x_cate1_enc"),
            udf("x_desc1").alias("x_desc1_enc")
        )
    )

    count = tokenize_df.count()
    repart_num = count // 100000
    if repart_num < 64:
        repart_num = 64
    elif repart_num > 300:
        repart_num = 300

    tokenize_df.repartition(repart_num).write.format(output_format).mode("overwrite").save(output_path)

    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Analyze char task.')
    parser.add_argument('--core', type=str,
                        default="16",
                        help="how many worker for spark.")
    parser.add_argument('--memory', type=str,
                        default="54",
                        help="how much memory for spark.")
    parser.add_argument('--input_path', type=str,
                        default="",
                        help="json2 path in S3 in parquet format.")
    parser.add_argument('--output_path', type=str,
                        default="",
                        help="S3 path, output result in 'output_format' format.")
    parser.add_argument(
        '--to_clean_char_file_path', type=str,
        default="",
        help=("S3 or local path[optional], path to char list txt file. "
              "format: each char in file seperated with special char '\x01'.")
    )
    parser.add_argument(
        '--bpe_merge_path', type=str,
        default="",
        help="S3 path[optional], bpe_merges.txt file path. If none using the one in 'resources'."
    )
    parser.add_argument(
        '--output_format', type=str,
        default="parquet",
        help=("string, output format, default: 'parquet'. "
              "Availible format: 'tfrecord', 'parquet', 'json', 'csv'")
    )

    args = parser.parse_args()
    logger = get_logger()
    logger.info("args: {}".format(args.__dict__))

    if not args.input_path:
        raise ValueError(f"input_path: {args.input_path} is not valid value.")
    if not args.output_path:
        raise ValueError(f"output_path: {args.output_path} is not valid value.")

    spark = get_stand_alone_spark(
        core=int(args.core),
        memory=int(args.memory)
    )

    exit_code = main(
        spark, logger,
        input_path=args.input_path,
        output_path=args.output_path,
        to_clean_char_file_path=args.to_clean_char_file_path,
        bpe_merge_path=args.bpe_merge_path,
        output_format=args.output_format
    )
    exit(exit_code)
