import argparse
import unicodedata

from utils.spark_utils import get_stand_alone_spark
from utils import s3_utils
from utils.logging_conf import get_logger


def write_char_list(path, char_list):
    if path.startswith("s3"):
        s3_path = s3_utils.convert_to_s3a(path)
        bucket, key = s3_utils.extract_bucket_and_key(s3_path)
        data = "\x01".join(char_list).encode()
        s3_utils.s3_upload_binary(data, bucket, key)
    else:
        # write to local path
        with open(path, "w") as f:
            f.write("\x01".join(char_list))

    return char_list


def get_chars_in_category(char_row_list):
    to_clean_chars = []
    for row in char_row_list:
        char = row["char"]
        char_cate = unicodedata.category(char)
        if char_cate.startswith("P") or char_cate.startswith("Z") or char_cate.startswith("C"):
            to_clean_chars.append(char)

    return to_clean_chars


def main(spark, logger, input_path, output_path):
    """Generate char list to clean.
    Args:
        spark: spark session.
        logger: python logger.
        input_path: S3 path, json2 in parquet format.
        output_path: S3 path, file path to write char list.
            format: each char in file seperated with special char '\x01'.
    """
    logger.info("Input args:")
    logger.info(f"input_path: {input_path}")
    logger.info(f"output_path: {output_path}")
    df = spark.read.parquet(input_path)
    select_column = (
        df
        .selectExpr(
            "LOWER(TRIM(title)) as title",
            "LOWER(TRIM(x_cate1)) AS x_cate1",
            "LOWER(TRIM(x_desc1)) AS x_desc1",
        )
    )
    titles = (
        select_column
        .selectExpr("title AS text")
        .drop_duplicates()
    )

    cates = (
        select_column
        .selectExpr("x_cate1 AS text")
        .drop_duplicates()
    )

    desc = (
        select_column
        .selectExpr("x_desc1 AS text")
        .drop_duplicates()
    )

    total_text = titles.union(cates).union(desc)

    split_char = (
        total_text
        .selectExpr(
            "ARRAY_DISTINCT(SPLIT(text, '')) as chars"
        )
        .selectExpr(
            "EXPLODE(chars) as char"
        )
        .groupby("char")
        .count()
    )
    chars = split_char.sort("count", ascending=False).collect()
    to_clean_char_list = get_chars_in_category(chars)

    write_char_list(to_clean_char_list)

    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Analyze char task.')
    # Pre-defined args in driver task_runner.
    parser.add_argument('--input_path', type=str,
                        default="",
                        help="json2 path in S3 in parquet format.")
    parser.add_argument('--output_path', type=str,
                        default="",
                        help="file output path")
    parser.add_argument('--core', type=str,
                        default="12",
                        help="how many worker for spark.")
    parser.add_argument('--memory', type=str,
                        default="48",
                        help="how much memory for spark.")

    args = parser.parse_args()
    logger = get_logger()
    logger.info("args: {}".format(args.__dict__))

    if not args.input_path:
        raise ValueError(f"input_path: {args.input_path} is not valid value.")
    if not args.output_path:
        raise ValueError(f"output_path: {args.output_path} is not valid value.")

    spark = get_stand_alone_spark(
        core=int(args.core),
        memory=int(args.memory)
    )

    exit_code = main(spark, logger, input_path=args.input_path, output_path=args.output_path)
    exit(exit_code)
