import os
import re
import argparse

from pyspark.sql import functions as F

from utils.spark_utils import get_stand_alone_spark
from utils.logging_conf import get_logger
from utils import s3_utils
from utils.env import APP_HOME


def build_clean_char_re_str(char_list):
    re_char = "["

    if "|" not in char_list:
        char_list.append("|")
    if "\n" not in char_list:
        char_list.append("\n")

    for c in char_list:
        if c == "[":
            re_char += "\\["
        elif c == "]":
            re_char += "\\]"
        elif c == "^":
            re_char += "\\^"
        elif c == "-":
            re_char += "\\-"
        else:
            re_char += c

    re_char += "]"

    return re_char


def read_char_list(path):
    if path.startswith("s3"):
        s3_path = s3_utils.convert_to_s3a(path)
        bucket, key = s3_utils.extract_bucket_and_key(s3_path)
        data = s3_utils.s3_read_object(bucket, key)
        char_list = data.decode('utf-8').split("\x01")
    else:
        # read as local path
        with open(path, "r") as f:
            char_list = f.read().split("\x01")

    return char_list


def get_preprocess_udf(to_clean_char_path, merge_num=40000):
    char_list = read_char_list(to_clean_char_path)
    re_str = build_clean_char_re_str(char_list)
    re_com = re.compile(re_str)

    def preprocess(inp_str):
        # localize object
        re_compile = re_com
        if inp_str is None:
            return None

        # preprocess
        inp_str = inp_str.lower().strip()
        inp_str = re_compile.sub(" ", inp_str)
        inp_str = re.sub("[ ]{2,}", " ", inp_str).strip()

        return inp_str

    udf = F.udf(preprocess)

    return udf


def main(spark, logger, input_path, output_path, to_clean_char_file_path=None):
    """Clean char in 'to_clean_char_file_path'. Replace with white space.
    Args:
        spark: spark session.
        logger: python logger.
        input_path: S3 path, json2 in parquet format.
        output_path: S3 path, output result in parquet format.
        to_clean_char_file_path: S3 or local path[optional], path to char list txt file.
            format: each char in file seperated with special char '\x01'.
    """
    logger.info("Input args:")
    logger.info(f"input_path: {input_path}")
    logger.info(f"output_path: {output_path}")
    logger.info(f"to_clean_char_file_path: {to_clean_char_file_path}")
    if to_clean_char_file_path is None:
        logger.info(f"to_clean_char_file_path is empty: {to_clean_char_file_path}")
        to_clean_char_file_path = os.path.join(APP_HOME, "resources/to_clean_char_list.txt")
        logger.info(f"Read from local path: {to_clean_char_file_path}")

    preprocess_udf = get_preprocess_udf(to_clean_char_file_path)

    df = spark.read.parquet(input_path)

    clean_df = (
        df
        .select(
            F.col("groupId").alias("offer_id"),
            "merchant",
            preprocess_udf("title").alias("title"),
            preprocess_udf("x_cate1").alias("x_cate1"),
            preprocess_udf("x_desc1").alias("x_desc1"),
            F.trim(F.lower("brand")).alias("brand")
        )
    )

    count = clean_df.count()
    repart_num = count // 100000
    if repart_num < 64:
        repart_num = 64
    elif repart_num > 300:
        repart_num = 300

    clean_df.repartition(repart_num).write.parquet(output_path, mode="overwrite")

    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Analyze char task.')
    # Pre-defined args in driver task_runner.
    parser.add_argument('--input_path', type=str,
                        default="",
                        help="json2 path in S3 in parquet format.")
    parser.add_argument('--output_path', type=str,
                        default="",
                        help="file output path")
    parser.add_argument('--core', type=str,
                        default="12",
                        help="how many worker for spark.")
    parser.add_argument('--memory', type=str,
                        default="48",
                        help="how much memory for spark.")

    args = parser.parse_args()
    logger = get_logger()
    logger.info("args: {}".format(args.__dict__))

    if not args.input_path:
        raise ValueError(f"input_path: {args.input_path} is not valid value.")
    if not args.output_path:
        raise ValueError(f"output_path: {args.output_path} is not valid value.")

    spark = get_stand_alone_spark(
        core=int(args.core),
        memory=int(args.memory)
    )

    exit_code = main(spark, logger, input_path=args.input_path, output_path=args.output_path)
    exit(exit_code)
