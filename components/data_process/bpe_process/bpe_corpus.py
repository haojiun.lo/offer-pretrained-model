import argparse

from utils.spark_utils import get_stand_alone_spark
from utils.logging_conf import get_logger


def main(spark, logger, input_path, output_path):
    """Generate BPE corpus for training.
    Args:
        spark: spark session.
        logger: python logger.
        input_path: S3 path, json2 in parquet format.
        output_path: S3 path, path to write output.
    """
    logger.info("Input args:")
    logger.info(f"input_path: {input_path}")
    logger.info(f"output_path: {output_path}")
    df = spark.read.parquet(input_path)
    # Stack title, x_cate1 and x_desc1
    text = (
        df
        .selectExpr(
            "title as text"
        )
    )

    cate = (
        df
        .selectExpr(
            "EXPLODE(SPLIT(x_cate1, ' ')) AS text"
        )
    )

    desc = (
        df
        .selectExpr(
            "x_desc1 as text"
        )
    )

    total_text = (
        text
        .unionByName(cate)
        .unionByName(desc)
        .where("text IS NOT NULL AND text != ''")
        .groupby("text")
        .count()
    )

    count = total_text.count()
    repart_num = count // 100000
    if repart_num < 64:
        repart_num = 64
    elif repart_num > 300:
        repart_num = 300

    total_text.sort("text").repartition(repart_num).write.parquet(output_path, mode="overwrite")

    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Analyze char task.')
    # Pre-defined args in driver task_runner.
    parser.add_argument('--input_path', type=str,
                        default="",
                        help="json2 path in S3 in parquet format.")
    parser.add_argument('--output_path', type=str,
                        default="",
                        help="file output path in S3")
    parser.add_argument('--core', type=str,
                        default="12",
                        help="how many worker for spark.")
    parser.add_argument('--memory', type=str,
                        default="48",
                        help="how much memory for spark.")

    args = parser.parse_args()
    logger = get_logger()
    logger.info("args: {}".format(args.__dict__))

    if not args.input_path:
        raise ValueError(f"input_path: {args.input_path} is not valid value.")
    if not args.output_path:
        raise ValueError(f"output_path: {args.output_path} is not valid value.")

    spark = get_stand_alone_spark(
        core=int(args.core),
        memory=int(args.memory)
    )

    exit_code = main(spark, logger, input_path=args.input_path, output_path=args.output_path)
    exit(exit_code)
