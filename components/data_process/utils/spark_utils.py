import os

from pyspark.sql import SparkSession


APP_HOME = os.environ.get("APP_HOME", "/opt/spark/work-dir")


def get_stand_alone_spark(
        core: int = 16,
        memory: int = 56,
        local_dir: str = None,
        aws_assumed_role: str = None,
        **extra_spark_configs):
    """
    Args:
        core: number of spark worker in local machine. Normally one spark worker utilize one cpu core.
        memory: memory for spark in gigabyes.
        local_dir: optional, local file system path for temporary files.
            Default would be '/opt/spark/work-dir/spark_temp'
        aws_assumed_role: optional, aws role to assume for S3 access.
        other_spark_configs: key-value for spark configs.
    """
    spark_builder = (
        SparkSession.builder.master(f"local[{core}]")
        .config("spark.driver.memory", f"{memory}g")
        .config("spark.cleaner.referenceTracking.cleanCheckpoints", True)
    )
    if local_dir:
        local_path = local_dir
    else:
        local_path = os.path.join(APP_HOME, "spark_temp")
    spark_builder = spark_builder.config("spark.local.dir", local_path)

    if extra_spark_configs:
        for key, value in extra_spark_configs.items():
            spark_builder = spark_builder.config(key, value)

    return spark_builder.getOrCreate()
