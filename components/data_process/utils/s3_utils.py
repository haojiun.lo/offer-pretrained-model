import os

import boto3
import botocore


def convert_to_s3a(s3_path):
    if s3_path.startswith("s3://"):
        sep = s3_path.split("s3://")
        return "".join(["s3a://"], sep[1])
    else:
        return s3_path


def extract_bucket_and_key(s3a_path):
    remove_s3a = s3a_path.split("s3a://")[1]
    sep = remove_s3a.split("/")

    bucket = sep[0]
    key = "/".join(sep[1:])

    return bucket, key


def s3_list_prefix(bucket, prefix):
    s3 = boto3.resource('s3')
    bucket_obj = s3.Bucket(bucket)
    list_summary = list(bucket_obj.objects.filter(Prefix=prefix))

    list_prefix = [summary.key for summary in list_summary]

    return list_prefix


def s3_read_object(bucket, key):
    s3 = boto3.resource('s3')

    return s3.Object(bucket, key).get()["Body"].read()


def s3_upload_binary(binary, bucket, key):
    s3 = boto3.resource('s3')
    bucket_obj = s3.Bucket(bucket)
    bucket_obj.put_object(Key=key, Body=binary)


def s3_upload_file(local_file, bucket, key):
    s3 = boto3.resource('s3')
    bucket_obj = s3.Bucket(bucket)
    with open(local_file, "rb") as f:
        bucket_obj.put_object(Key=key, Body=f)


def s3_delete_file(bucket, key):
    s3 = boto3.resource('s3')
    s3.Object(bucket, key).delete()


def s3_path_exist_boto(bucket, key):
    s3 = boto3.resource('s3')
    try:
        s3.Object(bucket, key).load()
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            return False
        else:
            raise
    else:
        return True


def s3_path_exists(spark, path):
    sc = spark.sparkContext
    fs = sc._jvm.org.apache.hadoop.fs.FileSystem.get(
        sc._jvm.java.net.URI.create(path),
        sc._jsc.hadoopConfiguration()
    )
    return fs.exists(sc._jvm.org.apache.hadoop.fs.Path(path))


def download_s3_folder(bucket_name, s3_folder, local_dir=None, logger=None):
    """
    From : https://stackoverflow.com/questions/49772151/download-a-folder-from-s3-using-boto3
    Download the contents of a folder directory
    Args:
        bucket_name: the name of the s3 bucket
        s3_folder: the folder path in the s3 bucket
        local_dir: a relative or absolute directory path in the local file system
    """
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)
    for obj in bucket.objects.filter(Prefix=s3_folder):
        target = obj.key if local_dir is None \
            else os.path.join(local_dir, os.path.relpath(obj.key, s3_folder))
        if not os.path.exists(os.path.dirname(target)):
            os.makedirs(os.path.dirname(target))
        if obj.key[-1] == '/':
            continue
        if logger:
            logger.info(f"Downloading {obj.key} to {target}")
        bucket.download_file(obj.key, target)
