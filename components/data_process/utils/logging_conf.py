import os
import time
import logging

# TODO: make this more organized.
logger = None
log_file_path = None
log_filename = None
log_dir = None
err_filename = None
err_file_path = None


def get_logger(working_dir=None, job_name=None):
    global logger, log_filename, log_file_path, log_dir, java_log_path
    global err_filename, err_file_path
    if logger:
        return logger
    if working_dir:
        log_dir = os.path.join(working_dir, "log")
    else:
        log_dir = "/data/data_process/log/"
    if job_name is None:
        job_name = "log-etl"

    # Create formatter
    formatter = logging.Formatter(
        "%(levelname)s:%(asctime)s %(filename)s:%(lineno)d %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S")

    # Create console handler
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    ch.setFormatter(formatter)

    # Create file handler
    timestamp = str(int(time.time()))  # create time stamp for log file name.
    log_filename = "{}_log_{}.txt".format(job_name, timestamp)
    log_file_path = os.path.join(log_dir, log_filename)
    err_filename = "{}_err_{}.txt".format(job_name, timestamp)
    err_file_path = os.path.join(log_dir, err_filename)

    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)
    fh = logging.FileHandler(log_file_path)
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)

    # Create logger
    logger = logging.getLogger('root')
    logger.setLevel(logging.DEBUG)

    # add handler to logger
    logger.addHandler(ch)
    logger.addHandler(fh)

    logger.info(f"Log file path: {log_file_path}")

    return logger
