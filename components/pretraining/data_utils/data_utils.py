import functools

import tensorflow as tf

from dataclasses import dataclass, field
from typing import List


@dataclass(frozen=True)
class TokenizerInfo:
    bos_token_id: int
    eos_token_id: int
    mask_token_id: int
    sep_token_id: int
    pad_token_id: int
    vocab_size: int
    all_special_ids: List = field(init=False)

    def __post_init__(self):
        self.all_special_ids = list(set([
            self.bos_token_id, self.eos_token_id, self.mask_token_id,
            self.mask_token_id, self.sep_token_id, self.pad_token_id
         ]))


def _parse_function(example_proto):
    feature_description = {
        'merchant': tf.io.FixedLenFeature([], tf.string, default_value=""),
        'brand_id': tf.io.FixedLenFeature([], tf.int64, default_value=-100),
        'merchant_cate_id': tf.io.FixedLenFeature([], tf.int64, default_value=-100),
        'title_enc': tf.io.FixedLenSequenceFeature([], tf.int64, allow_missing=True),
        'x_desc1_enc': tf.io.FixedLenSequenceFeature([], tf.int64, allow_missing=True),
        'cate_str_enc': tf.io.FixedLenSequenceFeature([], tf.int64, allow_missing=True),
    }
    return tf.io.parse_single_example(example_proto, feature_description)


def get_combine_inp_fn(tokenizer_info, max_inp_len=256):

    def combine_inp(example_proto, max_inp_len, tokenizer_info):
        title_enc = example_proto["title_enc"]
        desc_enc = example_proto["x_desc1_enc"]
        cate_enc = example_proto["cate_str_enc"]

        del example_proto["title_enc"]
        del example_proto["x_desc1_enc"]
        del example_proto["cate_str_enc"]

        desc_rand1 = tf.random.uniform([1], maxval=1.0)

        rand_desc_enc1 = tf.cond(
            desc_rand1 < 0.5, lambda: desc_enc, lambda: tf.constant([], dtype=desc_enc.dtype))

        combined_cate = tf.concat(
            [
                [tokenizer_info.bos_token_id],
                title_enc, [tokenizer_info.sep_token_id], cate_enc, [tokenizer_info.sep_token_id], rand_desc_enc1
            ],
            axis=0
        )[:max_inp_len - 1]
        combined_cate = tf.concat([combined_cate, [tokenizer_info.eos_token_id]], axis=0)

        cate_pos_cate = tf.concat(
            [
                [0], tf.zeros_like(title_enc), [0],  # title
                tf.ones_like(cate_enc), [1],  # category
                tf.ones_like(rand_desc_enc1) * 2, [2]  # description
            ],
            axis=0
        )[:max_inp_len]

        example_proto["input_ids"] = tf.cast(combined_cate, tf.int64)

        example_proto["token_type_ids"] = tf.cast(cate_pos_cate, tf.int64)

        return example_proto

    combine_inp_fn = functools.partial(combine_inp, tokenizer_info=tokenizer_info, max_inp_len=max_inp_len)

    return combine_inp_fn


def get_mlm_token_fn(tokenizer_info, mask_prob=0.1):

    def tf_bernoulli(shape, probability):
        prob_matrix = tf.fill(shape, probability)
        return tf.cast(prob_matrix - tf.random.uniform(shape, 0, 1) >= 0, tf.bool)

    def tf_mask_tokens(inputs, tokenizer_info, mlm_probability):
        """
        Prepare masked tokens inputs/labels for masked language modeling: 80% MASK, 10% random, 10% original.
        """
        # TODO: not sure if there is better way to do this
        special_tokens_mask = tf.zeros_like(inputs, dtype=tf.bool)
        for i in tokenizer_info.all_special_ids:
            special_tokens_mask = tf.where(inputs == i, True, False) | special_tokens_mask

        input_shape = tf.shape(inputs)
        # 1 for a special token, 0 for a normal token in the special tokens mask
        # We sample a few tokens in each sequence for MLM training (with probability `self.mlm_probability`)
        masked_indices = tf_bernoulli(input_shape, mlm_probability) & ~special_tokens_mask
        # Replace unmasked indices with -100 in the labels since we only compute loss on masked tokens
        labels = tf.where(masked_indices, inputs, -100)

        # 80% of the time, we replace masked input tokens with tokenizer_info.mask_token ([MASK])
        indices_replaced = tf_bernoulli(input_shape, 0.8) & masked_indices

        inputs = tf.where(indices_replaced, tf.cast(tokenizer_info.mask_token_id, tf.int64), inputs)

        # 10% of the time, we replace masked input tokens with random word
        indices_random = tf_bernoulli(input_shape, 0.1) & masked_indices & ~indices_replaced
        random_words = tf.random.uniform(input_shape, maxval=tokenizer_info.vocab_size, dtype=tf.int64)
        inputs = tf.where(indices_random, random_words, tf.cast(inputs, dtype=tf.int64))

        # The rest of the time (10% of the time) we keep the masked input tokens unchanged
        return inputs, labels

    def mlm_token_fn(example_proto, tokenizer_info, mlm_probability):
        input_ids = example_proto["input_ids"]

        input_ids, labels = tf_mask_tokens(
            input_ids, tokenizer_info, mlm_probability)

        example_proto["input_ids"] = input_ids
        example_proto["mlm_labels"] = labels

        return example_proto

    ret_fn = functools.partial(
        mlm_token_fn, tokenizer_info=tokenizer_info, mlm_probability=mask_prob)

    return ret_fn


def get_pad_fn(tokenizer_info, inp_len=64):

    def _pad_inp(input_ids, labels, cate_pos, inp_len, PAD_id):
        pad_len = inp_len - tf.shape(input_ids)[0]
        inp_padded = tf.pad(input_ids, [[0, pad_len]], constant_values=PAD_id)
        cate_pos_padded = tf.pad(cate_pos, [[0, pad_len]], constant_values=3)
        labels_padded = tf.pad(labels, [[0, pad_len]], constant_values=-100)
        attn_mask = tf.concat(
            [tf.ones_like(input_ids, dtype=tf.float32), tf.zeros(pad_len, dtype=tf.float32)],
            axis=0
        )

        return inp_padded, labels_padded, cate_pos_padded, attn_mask

    def pad_inp(example_proto, inp_len, PAD_id):
        input_ids = example_proto["input_ids"]
        labels = example_proto["mlm_labels"]

        token_type_ids = example_proto["token_type_ids"]

        input_ids, labels, token_type_ids, attention_mask = _pad_inp(
            input_ids, labels, token_type_ids, inp_len, PAD_id)

        example_proto["input_ids"] = input_ids
        example_proto["mlm_labels"] = labels
        example_proto["token_type_ids"] = token_type_ids
        example_proto["attention_mask"] = attention_mask

        return example_proto

    pad_inp_fn = functools.partial(
        pad_inp, inp_len=inp_len, PAD_id=tokenizer_info.pad_token_id)

    return pad_inp_fn


def prepare_data(file_list, tokenizer_info, max_seq_length=64, mlm_probability=0.1):
    combine_inp_fn = get_combine_inp_fn(tokenizer_info, max_seq_length)
    mlm_fn = get_mlm_token_fn(tokenizer_info, mlm_probability)
    pad_fn = get_pad_fn(inp_len=max_seq_length, tokenizer_info=tokenizer_info)

    tf_dataset = tf.data.TFRecordDataset(file_list)
    tf_dataset = tf_dataset.map(_parse_function)
    tf_dataset = tf_dataset.map(combine_inp_fn)
    tf_dataset = tf_dataset.map(mlm_fn)
    tf_dataset = tf_dataset.map(pad_fn)

    return tf_dataset
