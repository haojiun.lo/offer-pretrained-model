import json
import logging
import math
import shutil
import os
import sys
from dataclasses import dataclass, field
from pathlib import Path
from typing import Optional
from tensorflow.python.keras import backend as K

import tensorflow as tf

import transformers
from transformers import (
    CONFIG_NAME,
    TF2_WEIGHTS_NAME,
    HfArgumentParser,
    TFTrainingArguments,
    create_optimizer,
    set_seed,
)
from prelnbert import TFPreLNBertForPreTraining, PreLNBertConfig, PreLNBertTokenizer
from data_utils import prepare_data, TokenizerInfo

logger = logging.getLogger(__name__)


# region Command-line arguments
@dataclass
class ModelArguments:
    """
    Arguments pertaining to which model/config/tokenizer we are going to fine-tune, or train from scratch.
    """
    use_pre_ln: bool = field(
        default=True,
        metadata={
            "help": (
                "Whether to use pre layernorm bert. This script only support this model type for now"
            )
        },
    )
    model_name_or_path: Optional[str] = field(
        default=None,
        metadata={
            "help": (
                "The model checkpoint for weights initialization.Don't set if you want to train a model from scratch."
            )
        },
    )
    config_overrides: Optional[str] = field(
        default=None,
        metadata={
            "help": (
                "Override some existing default config settings when a model is trained from scratch. Example: "
                "n_embd=10,resid_pdrop=0.2,scale_attn_weights=false,summary_type=cls_index"
            )
        },
    )
    config_name: Optional[str] = field(
        default=None, metadata={"help": "Pretrained config name or path if not the same as model_name"}
    )
    tokenizer_name: Optional[str] = field(
        default=None, metadata={"help": "Pretrained tokenizer name or path if not the same as model_name"}
    )
    cache_dir: Optional[str] = field(
        default=None,
        metadata={"help": "Where do you want to store the pretrained models downloaded from huggingface.co"},
    )

    def __post_init__(self):
        if self.config_overrides is not None and (self.config_name is not None or self.model_name_or_path is not None):
            raise ValueError(
                "--config_overrides can't be used in combination with --config_name or --model_name_or_path"
            )


@dataclass
class DataTrainingArguments:
    """
    Arguments pertaining to what data we are going to input our model for training and eval.
    """
    train_dir: Optional[str] = field(default=None, metadata={"help": "The dir of all input training data file (tfrecord)."})
    validation_dir: Optional[str] = field(
        default=None,
        metadata={"help": "An optional dir of all input cal data file (tfrecord)."},
    )
    overwrite_cache: bool = field(
        default=False, metadata={"help": "Overwrite the cached training and evaluation sets"}
    )
    max_seq_length: Optional[int] = field(
        default=64,
        metadata={
            "help": (
                "The maximum total input sequence length after tokenization. Sequences longer "
                "than this will be truncated."
            )
        },
    )
    preprocessing_num_workers: Optional[int] = field(
        default=None,
        metadata={"help": "The number of processes to use for the preprocessing."},
    )
    mlm_probability: float = field(
        default=0.15, metadata={"help": "Ratio of tokens to mask for masked language modeling loss"}
    )
    steps_per_epoch: int = field(
        default=10,
        metadata={
            "help": (
                "Total number of steps (batches of samples) before declaring one epoch finished"
                "and starting the next epoch."
            )
        }
    )
    validation_steps: int = field(
        default=10,
        metadata={
            "help": (
                "Total number of steps (batches of samples) to draw before stopping when performing"
                "validation at the end of every epoch."
            )
        }
    )
    log_dir: str = field(
        default="./logs",
        metadata={
            "help": "the path of the directory where to save the log files to be parsed by TensorBoard."
            }
        )

    def __post_init__(self):
        if self.train_dir is None or self.validation_dir is None:
            raise ValueError("Need training/validation file.")


class LRTensorBoard(tf.keras.callbacks.TensorBoard):
    # TODO: log num of step
    # add other arguments to __init__ if you need
    def __init__(self, log_dir, **kwargs):
        super().__init__(log_dir=log_dir, **kwargs)

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        step = self.model.optimizer.iterations
        logs.update({'steps': step})
        logs.update({'lr': K.eval(self.model.optimizer.lr(step))})
        super().on_epoch_end(epoch, logs)


class ModelCheckpoint(tf.keras.callbacks.Callback):
    def __init__(self, filepath, save_total_limit=3, **kwargs):
        self.filepath = filepath
        self.save_total_limit = save_total_limit
        self.save_times = 0

    def on_epoch_end(self, epoch, logs=None):
        self.model.save_pretrained(os.path.join(self.filepath, f"ckpt-{epoch}"))
        self.save_times += 1
        if self.save_times - self.save_total_limit == 1:
            checkpoints = sorted(tf.io.gfile.glob(os.path.join(self.filepath, "ckpt-*")))
            shutil.rmtree(checkpoints[0])
            self.save_times -= 1


def main():
    # region Argument Parsing
    parser = HfArgumentParser((ModelArguments, DataTrainingArguments, TFTrainingArguments))
    if len(sys.argv) == 2 and sys.argv[1].endswith(".json"):
        # If we pass only one argument to the script and it's the path to a json file,
        # let's parse it to get our arguments.
        model_args, data_args, training_args = parser.parse_json_file(json_file=os.path.abspath(sys.argv[1]))
    else:
        model_args, data_args, training_args = parser.parse_args_into_dataclasses()

    if model_args.use_pre_ln:
        config_class = PreLNBertConfig
        model_class = TFPreLNBertForPreTraining
        tokenizer_class = PreLNBertTokenizer
    else:
        raise NotImplementedError("Other model type is not yet implemented for this task")

    if training_args.output_dir is not None:
        training_args.output_dir = Path(training_args.output_dir)
        os.makedirs(training_args.output_dir, exist_ok=True)

    # endregion

    # region Checkpoints
    # Detecting last checkpoint.
    checkpoint = None
    if len(os.listdir(training_args.output_dir)) > 0 and not training_args.overwrite_output_dir:
        config_path = training_args.output_dir / CONFIG_NAME
        weights_path = training_args.output_dir / TF2_WEIGHTS_NAME
        if config_path.is_file() and weights_path.is_file():
            checkpoint = training_args.output_dir
            logger.warning(
                f"Checkpoint detected, resuming training from checkpoint in {training_args.output_dir}. To avoid this"
                " behavior, change the `--output_dir` or add `--overwrite_output_dir` to train from scratch."
            )
        else:
            raise ValueError(
                f"Output directory ({training_args.output_dir}) already exists and is not empty. "
                "Use --overwrite_output_dir to continue regardless."
            )

    # endregion

    # region Setup logging
    # accelerator.is_local_main_process is only True for one process per machine.
    logger.setLevel(logging.INFO)
    transformers.utils.logging.set_verbosity_info()
    # endregion

    # If passed along, set the training seed now.
    if training_args.seed is not None:
        set_seed(training_args.seed)

    # region Load pretrained model and tokenizer
    #
    # In distributed training, the .from_pretrained methods guarantee that only one local process can concurrently
    # download model & vocab.

    if checkpoint is not None:
        config = config_class.from_pretrained(checkpoint)
    elif model_args.config_name:
        config = config_class.from_pretrained(model_args.config_name)
    elif model_args.model_name_or_path:
        config = config_class.from_pretrained(model_args.model_name_or_path)
    else:
        raise ValueError(
            "You are instantiating a new config from scratch. This is not supported by this script."
            "You can do it from another script, save it, and load it from here, using --config_name."
        )

    if model_args.tokenizer_name:
        tokenizer = tokenizer_class.from_pretrained(model_args.tokenizer_name)
    elif model_args.model_name_or_path:
        tokenizer = tokenizer_class.from_pretrained(model_args.model_name_or_path)
    else:
        raise ValueError(
            "You are instantiating a new tokenizer from scratch. This is not supported by this script."
            "You can do it from another script, save it, and load it from here, using --tokenizer_name."
        )

    if data_args.max_seq_length is None:
        max_seq_length = tokenizer.model_max_length
        if max_seq_length > 1024:
            logger.warning(
                f"The tokenizer picked seems to have a very large `model_max_length` ({tokenizer.model_max_length}). "
                "Picking 1024 instead. You can reduce that default value by passing --max_seq_length xxx."
            )
            max_seq_length = 1024
    else:
        if data_args.max_seq_length > tokenizer.model_max_length:
            logger.warning(
                f"The max_seq_length passed ({data_args.max_seq_length}) is larger than the maximum length for the"
                f"model ({tokenizer.model_max_length}). Using max_seq_length={tokenizer.model_max_length}."
            )
        max_seq_length = min(data_args.max_seq_length, tokenizer.model_max_length)

    # region Load datasets
    tokenizer_info = TokenizerInfo(
        tokenizer.bos_token_id, tokenizer.eos_token_id, tokenizer.mask_token_id,
        tokenizer.sep_token_id, tokenizer.pad_token_id, tokenizer.vocab_size)
    trn_file_list = tf.io.gfile.glob(os.path.join(data_args.train_dir, "*.tfrecord"))
    trn_dataset = prepare_data(trn_file_list, tokenizer_info, data_args.max_seq_length, data_args.mlm_probability)
    val_file_list = tf.io.gfile.glob(os.path.join(data_args.validation_dir, "*.tfrecord"))
    val_dataset = prepare_data(val_file_list, tokenizer_info, data_args.max_seq_length, data_args.mlm_probability)
    # endregion

    # Log a sample from the training set:
    logger.info(f"Sample of the training set: {next(iter(trn_dataset))}.")
    # endregion

    with training_args.strategy.scope():
        # region Prepare model
        if checkpoint is not None:
            model = model_class.from_pretrained(checkpoint, config=config)
        elif model_args.model_name_or_path:
            model = model_class.from_pretrained(model_args.model_name_or_path, config=config)
        else:
            logger.info("Training new model from scratch")
            model = model_class.from_config(config)

        model.resize_token_embeddings(len(tokenizer))
        # endregion

        # region TF Dataset preparation
        num_replicas = training_args.strategy.num_replicas_in_sync
        options = tf.data.Options()
        options.experimental_distribute.auto_shard_policy = tf.data.experimental.AutoShardPolicy.OFF

        tf_train_dataset = (trn_dataset
                            .shuffle(10000, reshuffle_each_iteration=True)
                            .batch(num_replicas * training_args.per_device_train_batch_size, drop_remainder=True)
                            .with_options(options)
                            )
        tf_eval_dataset = (val_dataset
                           .batch(num_replicas * training_args.per_device_eval_batch_size, drop_remainder=True)
                           .with_options(options)
                           )
        # endregion

        # region Optimizer and loss
        num_train_steps = int(data_args.steps_per_epoch) * int(training_args.num_train_epochs)
        if training_args.warmup_steps > 0:
            num_warmup_steps = training_args.warmup_steps
        elif training_args.warmup_ratio > 0:
            num_warmup_steps = int(num_train_steps * training_args.warmup_ratio)
        else:
            num_warmup_steps = 0

        # Bias and layernorm weights are automatically excluded from the decay
        optimizer, _ = create_optimizer(
            init_lr=training_args.learning_rate,
            num_train_steps=num_train_steps,
            num_warmup_steps=num_warmup_steps,
            adam_beta1=training_args.adam_beta1,
            adam_beta2=training_args.adam_beta2,
            adam_epsilon=training_args.adam_epsilon,
            weight_decay_rate=training_args.weight_decay,
            adam_global_clipnorm=training_args.max_grad_norm,
        )

        # no user-specified loss = will use the model internal loss
        model.compile(optimizer=optimizer, jit_compile=training_args.xla, run_eagerly=False)
        # endregion

        callbacks = [LRTensorBoard(data_args.log_dir, update_freq=1), ModelCheckpoint(training_args.output_dir)]

        # region Training and validation
        logger.info("***** Running training *****")
        logger.info(f"  Num steps per epoch = {int(data_args.steps_per_epoch)}")
        logger.info(f"  Num Epochs = {training_args.num_train_epochs}")
        logger.info(f"  Instantaneous batch size per device = {training_args.per_device_train_batch_size}")
        logger.info(f"  Total train batch size = {training_args.per_device_train_batch_size * num_replicas}")

        # TODO: Use custom training loop instead of using tf.keras model fit for more flexible usage
        # need to set validation_steps otherwise it will runs forever
        history = model.fit(
            tf_train_dataset,
            validation_data=tf_eval_dataset,
            steps_per_epoch=int(data_args.steps_per_epoch),
            epochs=int(training_args.num_train_epochs),
            validation_steps=int(data_args.validation_steps),
            callbacks=callbacks,
        )
        train_loss = history.history["loss"][-1]
        try:
            train_perplexity = math.exp(train_loss)
        except OverflowError:
            train_perplexity = math.inf
        logger.info(f"  Final train loss: {train_loss:.3f}")
        logger.info(f"  Final train perplexity: {train_perplexity:.3f}")

    validation_loss = history.history["val_loss"][-1]
    try:
        validation_perplexity = math.exp(validation_loss)
    except OverflowError:
        validation_perplexity = math.inf
    logger.info(f"  Final validation loss: {validation_loss:.3f}")
    logger.info(f"  Final validation perplexity: {validation_perplexity:.3f}")

    if training_args.output_dir is not None:
        output_eval_file = os.path.join(training_args.output_dir, "all_results.json")
        results_dict = dict()
        results_dict["train_loss"] = train_loss
        results_dict["train_perplexity"] = train_perplexity
        results_dict["eval_loss"] = validation_loss
        results_dict["eval_perplexity"] = validation_perplexity
        with open(output_eval_file, "w") as writer:
            writer.write(json.dumps(results_dict))
        # endregion

    if training_args.output_dir is not None:
        # If we're not pushing to hub, at least save a local copy when we're done
        model.save_pretrained(training_args.output_dir)


if __name__ == "__main__":
    main()
