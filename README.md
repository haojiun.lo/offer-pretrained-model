# Offer Pretrained Model

- repo structure
```
general components repo/
├── components/
│   ├── component_1/
│   │   ├── component.yaml
│   │   ├── Dockerfile
│   │   ├── my_component_1.py
│   │   └── test_my_component_1.py
│   ├── component_2/
│   │   └── ...
│   ├── component_3/
│   │   └── ...
│   ├── test.sh
│   └── utils.py
├── notebooks
├── .gitlab-ci.yml
└── README.md
```